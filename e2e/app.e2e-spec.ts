import { Angular4SchedulerPage } from './app.po';

describe('angular4-scheduler App', () => {
  let page: Angular4SchedulerPage;

  beforeEach(() => {
    page = new Angular4SchedulerPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
